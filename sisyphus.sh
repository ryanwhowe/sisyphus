#!/usr/bin/env bash
##############################################################################
##############################################################################
#                                                                            #
#  Sisyphus - Some Jobs are never F%*#^@# done!                              #
#                                                                            #
##############################################################################
##############################################################################
# @author: Ryan W. Howe <ryanwhowe@theaxerant.com>
# @since: 2018-05-18
#
# In Greek mythology Sisyphus or Sisyphos was the king of Ephyra.
# He was punished for his self-aggrandizing craftiness and deceitfulness
# by being forced to roll an immense boulder up a hill only for it to
# roll down when it nears the top, repeating this action for eternity.
# Through the classical influence on modern culture, tasks that are both
# laborious and futile are therefore described as Sisyphean.
##############################################################################

RANDOM=$$$(date +%s)
NORMAL=$(tput sgr0)

COLORS=(
$(tput setaf 1)   # RED
$(tput setaf 2)   # GREEN
$(tput setaf 3)   # YELLOW
$(tput setaf 190) # LIME_YELLOW
$(tput setaf 153) # POWDER_BLUE
$(tput setaf 4)   # BLUE
$(tput setaf 6)   # CYAN
$(tput setaf 7)   # WHITE
)

int_handler()
{
    echo ""
    echo "Processing Complete"
    # Kill the parent process of the script.
    kill $PPID
    exit 1
}
trap 'int_handler' INT

function message(){
    randval=$RANDOM
    COLOROUT=${COLORS[$randval % ${#COLORS[@]} ]}
    OUTCOLOR=$(( $randval % 6 ))
    if [ ${OUTCOLOR} -eq 0 ]; then
        echo ${COLOROUT}${1}${NORMAL}
    else
        echo ${NORMAL}${1}${NORMAL}
    fi
	rand=$(bc -l <<< "($randval  / 65534)") #32767)")
	sleep ${rand}
}

function dosearch(){
    find ${1} -type f -print0 | while IFS= read -r -d '' file; do
        message "Processing ... ${file}"
    done

}
if [ $# -eq 0 ]; then
    SEARCH_DIR="/usr/"
else
    SEARCH_DIR=${1}
fi

# trim all trailing slashes if entered
SEARCH_DIR=$(echo "$SEARCH_DIR" | sed 's:/*$::')

while :
do
    dosearch $SEARCH_DIR
done
