# sisyphus

Some Jobs are never F%*#^@# done!   

In Greek mythology Sisyphus or Sisyphos was the king of Ephyra. He was punished for his self-aggrandizing craftiness and deceitfulness by being forced to roll an immense boulder up a hill only for it to roll down when it nears the top, repeating this action for eternity. Through the classical influence on modern culture, tasks that are both laborious and futile are therefore described as Sisyphean.

This is a fake script to give the appearance of work being performed.  I developed this when I was working directly for a sales team and needed a break, I would run this on several different terminal windows to give the appearance of being "to busy for random questions" and it worked very well.

This is a shell script that will endlessly loop through a file list with the string "Processing ... " prepended to the fully qualified file name.  

There are random pauses placed between each file to give the allusion of work being performed on each file, there is also random coloring of the output randomly used.

Usage
-----
<code>
sisyphus.sh (directory)
</code> 

the (directory) is optional, if omitted the /usr/ directory is used.  You should only use a directory that you have read
access to the entire tree.